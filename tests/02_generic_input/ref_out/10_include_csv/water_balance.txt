"time [s]"	"region"	"quantity [m(3)]"	"flux"	"flux_in"	"flux_out"	"mass"	"source"	"source_in"	"source_out"	"flux_increment"	"source_increment"	"flux_cumulative"	"source_cumulative"	"error"
0	"channel_1d"	"water_volume"	0	0	0	0	0	0	0	0	0	0	0	0
0	"fracture_2d_1"	"water_volume"	0	0	0	0	0	0	0	0	0	0	0	0
0	"fracture_2d_2"	"water_volume"	0	0	0	0	0	0	0	0	0	0	0	0
0	"cube_3d"	"water_volume"	0	0	0	0	0	0	0	0	0	0	0	0
0	".channel_1d"	"water_volume"	-2.12997e-05	0	-2.12997e-05	0	0	0	0	0	0	0	0	0
0	".fracture_2d_1"	"water_volume"	3.72725e-05	1.05302	-1.05299	0	0	0	0	0	0	0	0	0
0	".fracture_2d_2"	"water_volume"	-8.7672e-06	1.0528	-1.05281	0	0	0	0	0	0	0	0	0
0	".cube_3d"	"water_volume"	-7.20555e-06	0.100887	-0.100894	0	0	0	0	0	0	0	0	0
